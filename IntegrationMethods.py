#!/usr/bin/env python
import requests
from requests.auth import HTTPBasicAuth
import xml.etree.ElementTree as ET
import xml.dom.minidom
import json

##################### GENERAL methods
def readConfFile():
    with open("environment_configuration.json") as db:
        env_conf = json.loads(db.read())
    return env_conf

def writeConfFile(data):
    with open("environment_configuration.json", 'w') as outfile:
        outfile.write(json.dumps(data, indent=4, sort_keys=True))


    #inst = form.instance_comboBox.currentText()
    #env = form.environment_comboBox.currentText()
    #opt_username = form.gui_username.displayText()
    #opt_password = form.gui_password.displayText()
def GuiConnectionInfo(**kwargs):
    #USAGE: GuiCredentails(inst='ESS6',env='DEV',opt_username='',opt_password='')
    print kwargs
    inst = kwargs['inst']
    env = kwargs['env']
    options = readConfFile()
    webappurl = options['environments'][inst][env]['webappurl']
    guiuser = options['intranet']['username']
    guipassword = options['intranet']['password']

    wsdlConn = {'url':'https://' + webappurl + '/meaweb/services/' + kwargs['service'],'username':guiuser,'password':guipassword}
    if kwargs['opt_username'] and kwargs['opt_password']:
        wsdlConn['username'] = kwargs['opt_username']
        wsdlConn['password'] = kwargs['opt_password']
    return wsdlConn


def sendEnvelope(env, payload, soap_ver='SOAP12'):
    if soap_ver == 'SOAP12':
        header = {'Content-Type': 'application/soap+xml', 'Accept': 'application/soap+xml', "SOAPAction": "urn:processDocument"}
    else:
        header = {'Content-Type': 'text/xml', 'Accept': 'text/xml', "SOAPAction": "urn:processDocument"}  #TODO correct header for soap11
    r = requests.post(env['url'], auth=HTTPBasicAuth(env['username'], env['password']), data=payload, headers=header, verify=False)
    print(r.status_code)
    print(r.content)
    pretty_xml_as_string = xml.dom.minidom.parseString(r.content).toprettyxml()
    print pretty_xml_as_string
    return r.status_code, r.content


############### SPECIFIC methods
def prepareQueryEnvelope(source_userid,service_name, soap_ver='SOAP12'):
    if soap_ver == 'SOAP12':
        soap_namespace = "http://www.w3.org/2003/05/soap-envelope"
    else:
        soap_namespace = "http://schemas.xmlsoap.org/soap/envelope/"

    ET._namespace_map[soap_namespace] = "soap"
    ET._namespace_map["http://www.ibm.com/maximo"] = "max"

    envelope = ET.Element('{' + soap_namespace + '}Envelope')
    body = ET.SubElement(envelope,'soap:Body')
    service = ET.SubElement(body,'{http://www.ibm.com/maximo}Query' + service_name)
    set = ET.SubElement(service,'max:' + service_name + 'Query')
    person = ET.SubElement(set,'max:PERSON')
    user = ET.SubElement(person,'max:MAXUSER')
    userid = ET.SubElement(user,'max:USERID')

    userid.text = source_userid

    payload = ET.tostring(envelope,encoding='utf-8')
    pretty_xml_as_string = xml.dom.minidom.parseString(payload).toprettyxml()
    print pretty_xml_as_string
    return payload


def getSourceUserDetails(payload):
    tree = ET.ElementTree(ET.fromstring(payload))
    root = tree.getroot()

    pluspcustvendor = root.findall('.//*/{http://www.ibm.com/maximo}PLUSPCUSTVENDOR')[0].text

    groups=[]
    for element in root.iter('{http://www.ibm.com/maximo}GROUPNAME'):
        groups.append(element.text)

    customers=[]
    for element in root.iter('{http://www.ibm.com/maximo}CUSTOMER'):
        customers.append(element.text)

    source_user_details = {'groups':groups, 'customers':customers, 'pluspcustvendor':pluspcustvendor}
    return source_user_details

def prepareSyncEnvelope(destination_userid,source_user_details,service_name,soap_ver='SOAP12'):
    if soap_ver == 'SOAP12':
        soap_namespace = "http://www.w3.org/2003/05/soap-envelope"
    else:
        soap_namespace = "http://schemas.xmlsoap.org/soap/envelope/"

    ET._namespace_map[soap_namespace] = "soap"
    ET._namespace_map["http://www.ibm.com/maximo"] = "max"

    envelope = ET.Element('{' + soap_namespace + '}Envelope')
    body = ET.SubElement(envelope,'soap:Body')
    service = ET.SubElement(body,'{http://www.ibm.com/maximo}Sync' + service_name)
    set = ET.SubElement(service,'max:' + service_name + 'Sync')
    person = ET.SubElement(set,'max:PERSON')
    personid = ET.SubElement(person,'max:PERSONID')
    user = ET.SubElement(person,'max:MAXUSER')
    userid = ET.SubElement(user,'max:USERID')
    pluspcustvendor = ET.SubElement(person,'max:PLUSPCUSTVENDOR')

    if source_user_details['pluspcustvendor']:
        pluspcustvendor.text = source_user_details['pluspcustvendor']

    if source_user_details['groups']:
        for group in source_user_details['groups']:
            groupuser = ET.SubElement(user,'max:GROUPUSER')
            groupname = ET.SubElement(groupuser,'max:GROUPNAME')
            groupname.text = group

    if source_user_details['customers']:
        for customer in source_user_details['customers']:
            plusppersoncust = ET.SubElement(person,'max:PLUSPPERSONCUST')
            customername = ET.SubElement(plusppersoncust,'max:CUSTOMER')
            customername.text = customer

    personid.text = destination_userid
    userid.text = destination_userid


    payload = ET.tostring(envelope,encoding='utf-8')
    pretty_xml_as_string = xml.dom.minidom.parseString(payload).toprettyxml()
    print pretty_xml_as_string

    return payload















#END
