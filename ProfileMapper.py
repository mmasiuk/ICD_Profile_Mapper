#!/usr/bin/env python
from PySide.QtCore import *
from PySide.QtGui import *
import sys
import layout
from datetime import date,datetime
import json
from IntegrationMethods import *

class MainDialog(QDialog, layout.Ui_mainDialog):

    def __init__(self, parent=None):
        super(MainDialog, self).__init__(parent)

        global destination_completer, source_completer, options

        options = readConfFile()
        instance_values = sorted(options['environments'].keys())
        environment_values = ['DEV','UAT','PROD']

        self.setupUi(self)
        self.connect(self.ProcessButton, SIGNAL("clicked()"), self.ProcessMapping)
        self.instance_comboBox.addItems(instance_values)
        self.environment_comboBox.addItems(environment_values)

####################################
        destination_completer = QCompleter(options['destination_list'], self)
        destination_completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.destination_userid_line.setCompleter(destination_completer)
        self.destination_userid_line.installEventFilter(self)

        source_completer = QCompleter(options['source_list'], self)
        source_completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.source_userid_line.setCompleter(source_completer)
        self.source_userid_line.installEventFilter(self)

    def eventFilter(self, object, event):
        if str(event.type()) == 'PySide.QtCore.QEvent.Type.MouseButtonPress' and object.objectName() == 'destination_userid_line' :
            destination_completer.complete()
            return True
        elif str(event.type()) == 'PySide.QtCore.QEvent.Type.MouseButtonPress' and object.objectName() == 'source_userid_line' :
            source_completer.complete()
            return True
        return QDialog.eventFilter(self,object,event)
####################################

    def ProcessMapping(self):
        self.progressBar.setValue(0)

        inst = self.instance_comboBox.currentText()
        env = self.environment_comboBox.currentText()
        opt_username = self.gui_username.displayText()
        opt_password = self.gui_password.displayText()
        source_userid = self.source_userid_line.displayText()
        destination_userid = self.destination_userid_line.displayText()
        service_name = 'MXISPERUSER'
        self.progressBar.setValue(20)

        if source_userid and destination_userid:
            if options['destination_list'].count(destination_userid) == 0:
                options['destination_list'].append(destination_userid)
                destination_completer = QCompleter(options['destination_list'], self)
                destination_completer.setCaseSensitivity(Qt.CaseInsensitive)
                self.destination_userid_line.setCompleter(destination_completer)
            if options['source_list'].count(source_userid) == 0:
                options['source_list'].append(source_userid)
                source_completer = QCompleter(options['destination_list'], self)
                source_completer.setCaseSensitivity(Qt.CaseInsensitive)
                self.source_userid_line.setCompleter(source_completer)

        writeConfFile(options)

        connection_info = GuiConnectionInfo(inst=inst,env=env,service=service_name,opt_username=opt_username,opt_password=opt_password)
        query_envelope = prepareQueryEnvelope(source_userid,service_name)
        #print query_envelope
        print connection_info
        try:
            query_response = sendEnvelope(connection_info,query_envelope)
            if query_response[0] != 200:
                query_envelope = prepareQueryEnvelope(source_userid,service_name, 'SOAP11')
                query_response = sendEnvelope(connection_info,query_envelope,'SOAP11')
        except:
            self.resultWindow.append('[' + str(datetime.today().strftime("%Y-%m-%d %H:%M:%S")) + '] Please verify whether intranet credentails or connection information for <b>' + inst + ' ' + env + '</b> are correct within <b>environment_configuration.json</b> file.')

        self.progressBar.setValue(50)
        try:
            source_user_details = getSourceUserDetails(query_response[1])
            sync_envelope = prepareSyncEnvelope(destination_userid,source_user_details,service_name)
            sync_reponse = sendEnvelope(connection_info,sync_envelope)
            if sync_reponse[0] != 200:
                sync_envelope = prepareSyncEnvelope(destination_userid,source_user_details,service_name,'SOAP11')
                sync_reponse = sendEnvelope(connection_info,sync_envelope,'SOAP11')

            if sync_reponse[0] == 200:
                self.resultWindow.append('[' + str(datetime.today().strftime("%Y-%m-%d %H:%M:%S")) + '] Profile transfered <b>SUCCESFULLY</b>.')
            else:
                self.resultWindow.append('[' + str(datetime.today().strftime("%Y-%m-%d %H:%M:%S")) + '] Problem occured during processing:')
                tree = ET.ElementTree(ET.fromstring(sync_reponse[1]))
                root = tree.getroot()
                faultstring = root.findall('.//*/faultstring')[0].text
                self.resultWindow.append(faultstring)
        except IndexError:
            self.resultWindow.append('[' + str(datetime.today().strftime("%Y-%m-%d %H:%M:%S")) + '] Source USERID: <b>' + self.source_userid_line.displayText() + '</b> \
            not found or service<b> ' + service_name + ' </b>do not exist on <b>' + inst + ' ' + env + '</b>')

        self.progressBar.setValue(100)


app = QApplication(sys.argv)
form = MainDialog()
form.show()
app.exec_()
