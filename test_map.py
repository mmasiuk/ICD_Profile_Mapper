import unittest

class TestMap(unittest.TestCase):
    """
    Test the add function from the calc library
    """

    def test_add_integers(self):
        """
        Test that the addition of two integers returns the correct total
        """
        result = 3
        self.assertEqual(result, 3)

if __name__ == '__main__':
    unittest.main()
