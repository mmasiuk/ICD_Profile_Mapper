# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'layout_concept.ui'
#
# Created: Mon Aug 28 14:15:10 2017
#      by: PyQt4 UI code generator 4.10.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_mainDialog(object):
    def setupUi(self, mainDialog):
        mainDialog.setObjectName(_fromUtf8("mainDialog"))
        mainDialog.resize(570, 385)
        self.ProcessButton = QtGui.QPushButton(mainDialog)
        self.ProcessButton.setGeometry(QtCore.QRect(320, 30, 131, 27))
        self.ProcessButton.setObjectName(_fromUtf8("ProcessButton"))
        self.resultWindow = QtGui.QTextBrowser(mainDialog)
        self.resultWindow.setGeometry(QtCore.QRect(10, 110, 291, 261))
        self.resultWindow.setObjectName(_fromUtf8("resultWindow"))
        self.resultLabel = QtGui.QLabel(mainDialog)
        self.resultLabel.setGeometry(QtCore.QRect(10, 90, 56, 16))
        self.resultLabel.setObjectName(_fromUtf8("resultLabel"))
        self.label_12 = QtGui.QLabel(mainDialog)
        self.label_12.setGeometry(QtCore.QRect(90, 10, 131, 71))
        self.label_12.setText(_fromUtf8(""))
        self.label_12.setObjectName(_fromUtf8("label_12"))

        self.pixmap = QtGui.QPixmap('./ibm_logo.png')
        self.label_12.setPixmap(self.pixmap.scaledToHeight(77))

        self.progressBar = QtGui.QProgressBar(mainDialog)
        self.progressBar.setGeometry(QtCore.QRect(460, 30, 111, 27))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))
        self.toolBox = QtGui.QToolBox(mainDialog)
        self.toolBox.setGeometry(QtCore.QRect(320, 70, 241, 301))
        self.toolBox.setFocusPolicy(QtCore.Qt.NoFocus)
        self.toolBox.setObjectName(_fromUtf8("toolBox"))
        self.instance = QtGui.QWidget()
        self.instance.setGeometry(QtCore.QRect(0, 0, 241, 243))
        self.instance.setObjectName(_fromUtf8("instance"))
        self.instance_comboBox = QtGui.QComboBox(self.instance)
        self.instance_comboBox.setGeometry(QtCore.QRect(0, 30, 241, 26))
        self.instance_comboBox.setObjectName(_fromUtf8("instance_comboBox"))
        self.environment_comboBox = QtGui.QComboBox(self.instance)
        self.environment_comboBox.setGeometry(QtCore.QRect(0, 90, 241, 26))
        self.environment_comboBox.setObjectName(_fromUtf8("environment_comboBox"))
        self.label_5 = QtGui.QLabel(self.instance)
        self.label_5.setGeometry(QtCore.QRect(0, 10, 91, 16))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.label_6 = QtGui.QLabel(self.instance)
        self.label_6.setGeometry(QtCore.QRect(0, 70, 91, 16))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.source_userid_line = QtGui.QLineEdit(self.instance)
        self.source_userid_line.setGeometry(QtCore.QRect(0, 150, 221, 26))
        self.source_userid_line.setReadOnly(False)
        self.source_userid_line.setObjectName(_fromUtf8("source_userid_line"))
        self.destination_userid_line = QtGui.QLineEdit(self.instance)
        self.destination_userid_line.setGeometry(QtCore.QRect(0, 210, 221, 26))
        self.destination_userid_line.setReadOnly(False)
        self.destination_userid_line.setObjectName(_fromUtf8("destination_userid_line"))
        self.label_7 = QtGui.QLabel(self.instance)
        self.label_7.setGeometry(QtCore.QRect(0, 130, 91, 16))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.label_8 = QtGui.QLabel(self.instance)
        self.label_8.setGeometry(QtCore.QRect(0, 190, 141, 16))
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.toolBox.addItem(self.instance, _fromUtf8(""))
        self.credentials = QtGui.QWidget()
        self.credentials.setGeometry(QtCore.QRect(0, 0, 241, 243))
        self.credentials.setObjectName(_fromUtf8("credentials"))
        self.gui_password = QtGui.QLineEdit(self.credentials)
        self.gui_password.setGeometry(QtCore.QRect(0, 80, 221, 26))
        self.gui_password.setObjectName(_fromUtf8("gui_password"))
        self.gui_username = QtGui.QLineEdit(self.credentials)
        self.gui_username.setGeometry(QtCore.QRect(0, 30, 221, 26))
        self.gui_username.setReadOnly(False)
        self.gui_username.setObjectName(_fromUtf8("gui_username"))
        self.label = QtGui.QLabel(self.credentials)
        self.label.setGeometry(QtCore.QRect(0, 10, 91, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(self.credentials)
        self.label_2.setGeometry(QtCore.QRect(0, 60, 91, 16))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.toolBox.addItem(self.credentials, _fromUtf8(""))

        self.retranslateUi(mainDialog)
        self.toolBox.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(mainDialog)

    def retranslateUi(self, mainDialog):
        mainDialog.setWindowTitle(_translate("mainDialog", "ICD Profile Mapper", None))
        self.ProcessButton.setText(_translate("mainDialog", "Start Processing", None))
        self.resultLabel.setText(_translate("mainDialog", "Result", None))
        self.label_5.setText(_translate("mainDialog", "Instance", None))
        self.label_6.setText(_translate("mainDialog", "Environment", None))
        self.label_7.setText(_translate("mainDialog", "Source Userid", None))
        self.label_8.setText(_translate("mainDialog", "Destination Userid", None))
        self.toolBox.setItemText(self.toolBox.indexOf(self.instance), _translate("mainDialog", "General", None))
        self.label.setText(_translate("mainDialog", "GUI username", None))
        self.label_2.setText(_translate("mainDialog", "GUI password", None))
        self.toolBox.setItemText(self.toolBox.indexOf(self.credentials), _translate("mainDialog", "Credentials", None))
